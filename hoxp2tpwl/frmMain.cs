﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;
using NHapiTools.Base.Net;
using System.IO;
using System.Globalization;

namespace hoxp2tpwl
{
    public partial class frmMain : Form
    {
        private CultureInfo en_us;
        private string m_hosxpconstr = System.Configuration.ConfigurationManager.AppSettings["hosxpconnectionstring"];
        private int m_interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["interval"]);
        private string m_worklistsserver = System.Configuration.ConfigurationManager.AppSettings["worklistserver"];
        private int m_worklistserverport = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["worklistport"]);
        private System.Timers.Timer m_timer = null;

        public frmMain()
        {
            InitializeComponent();
            en_us = new CultureInfo("en-US");
            m_timer = new System.Timers.Timer();
            m_timer.Interval = m_interval * 1000;
            m_timer.Elapsed += Ontimer;
          
        }

        private void Ontimer(Object source, System.Timers.ElapsedEventArgs e)
        {
            updatelog("Interval Start");
            
            List<worklistdata> result = getdatafromhosxp();

            if (result != null && result.Count >= 0) // have data
            {
                foreach (worklistdata wl in result)
                {
                    //create msg orm 01
                    string msgsend = getxrayordermessage(en_us, wl);
                    updatelog(String.Format("Create Worklist msg Detail HN:{0} Name:{1}", wl.hn, wl.pname + " " + wl.fname + " " + wl.lname));
                    //send msg to worklist server
                    SendORM_Message(msgsend,wl);
                    writetempfile(wl.xrayordernumber);
                }
               
            }
        }


        private void cmdStart_Click(object sender, EventArgs e)
        {
            if (cmdStart.Text == "Start")
            {
                cmdStart.Text = "Stop";
                m_timer.Start();
            }
            else
            {
                m_timer.Stop();
               cmdStart.Text = "Start";
            }
        }

        private void SendORM_Message(string message,worklistdata wl)
        {

            SimpleMLLPClient wlserver = null;
            try
            {
                wlserver = new SimpleMLLPClient(m_worklistsserver, m_worklistserverport);
                string result = wlserver.SendHL7Message(message);
                updatelog(String.Format("Send Message for HN:{0} Result:", wl.hn, result));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (wlserver != null)
                {
                    wlserver.Disconnect();
                    wlserver.Dispose();
                    wlserver = null;
                }
            }
        }

        private List<worklistdata> getdatafromhosxp()
        {
            MySqlConnection m_con = null;
            MySqlCommand m_cmd = null;
            MySqlDataReader m_rd = null;
            List<worklistdata> wllist = null;
            try
            {
                m_con = new MySqlConnection(m_hosxpconstr);
                m_cmd = new MySqlCommand();
                m_cmd.Connection = m_con;
                m_con.Open();

                string lastedxn = readtempfile();
                //edit nawang hospital
                //edit for suwankuha hostpital
                //m_cmd.CommandText = @"select b.hn,b.pname,b.fname,b.lname,b.birthday,c.name,d.xray_items_code,a.xray_list,a.order_date_time,a.xray_order_number,a.confirm_all from xray_head as a,patient as b,sex as c,xray_items as d where ((a.order_date_time >= CURDATE()) and (a.order_date_time < CURDATE() + INTERVAL 1 DAY)) and  a.hn=b.hn and b.sex = c.code and  a.xray_list = d.xray_items_name  order by a.order_date_time desc

                m_cmd.CommandText = String.Format("select b.hn,b.pname,b.fname,b.lname,b.birthday,c.name,d.xray_items_code,a.xray_list,a.order_date_time,a.xray_order_number,a.confirm_all from xray_head as a,patient as b,sex as c,xray_items as d where a.xray_order_number > {0} and  a.xray_list not like '%Dental%' and  a.hn=b.hn and b.sex = c.code and  a.xray_list = d.xray_items_name  order by a.xray_order_number asc", lastedxn);

                //m_cmd.CommandText = @"select b.hn,b.pname,b.fname,b.lname,b.birthday,c.name,d.xray_items_code,a.xray_list,a.order_date_time,a.xray_order_number,a.confirm_all from xray_head as a,patient as b,sex as c,xray_items as d where ((a.order_date_time >= CURDATE()) and (a.order_date_time < CURDATE() + INTERVAL 1 DAY)) and  a.hn=b.hn and b.sex = c.code and  a.xray_list = d.xray_items_name and a.confirm_all = 'N' and xray_list not like '%Dental%' order by a.order_date_time desc";
                //m_cmd.CommandText = @"select a.xn,b.hn,b.pname,b.fname,b.lname,b.birthday,c.name,a.xray_items_code,d.xray_items_name,a.request_date,a.request_time,a.examined_date from xray_report as a,patient as b,sex as c,xray_items d where a.request_date >=CURDATE() and a.request_date < DATE_ADD(CURDATE(), INTERVAL 1 day) and a.examined_date is null and a.hn=b.hn and b.sex = c.code and a.xray_items_code = d.xray_items_code order by a.request_date desc,a.request_time desc";
                //m_cmd.CommandText = string.Format("select a.xn,b.hn,b.pname,b.fname,b.lname,b.birthday,c.name,a.xray_items_code,d.xray_items_name,a.request_date,a.request_time,a.examined_date from xray_report as a,patient as b,sex as c,xray_items d where a.xn > {0} and a.hn=b.hn and b.sex = c.code and a.xray_items_code = d.xray_items_code order by a.request_date desc,a.request_time desc",lastxn);
                m_rd = m_cmd.ExecuteReader();
                wllist = new List<worklistdata>();

                while (m_rd.Read())
                {
                    worklistdata currwl = new worklistdata();


                    if (m_rd[0] != DBNull.Value)
                    {
                        currwl.hn = Convert.ToString(m_rd[0]).Trim();
                        currwl.hn = currwl.hn + "^^^TPACS";
                    }

                    if (m_rd[1] != DBNull.Value)
                    {
                        currwl.pname = Convert.ToString(m_rd[1]).Trim();
                    }

                    if (m_rd[2] != DBNull.Value)
                    {
                        currwl.fname = Convert.ToString(m_rd[2]).Trim();
                    }

                    if (m_rd[3] != DBNull.Value)
                    {
                        currwl.lname = Convert.ToString(m_rd[3]).Trim();
                    }

                    if (m_rd[4] != DBNull.Value)
                    {
                        currwl.birthday = Convert.ToDateTime(m_rd[4]);
                    }

                    if (m_rd[5] != DBNull.Value)
                    {
                        string sex = Convert.ToString(m_rd[5]).Trim();
                        if (sex == "ชาย")
                        {
                            currwl.sex = "M";

                        }
                        else
                        {
                            currwl.sex = "F";
                        }
                         
                    }

                    if (m_rd[6] != DBNull.Value)
                    {
                        currwl.xraycode = Convert.ToString(m_rd[6]).Trim();
                    }
                    if (m_rd[7] != DBNull.Value)
                    {
                        currwl.xraydesc = Convert.ToString(m_rd[7]).Trim();
                    }

                    if (m_rd[8] != DBNull.Value)
                    {
                        currwl.orderdate = Convert.ToDateTime(m_rd[8]);
                    }

                    if (m_rd[9] != DBNull.Value)
                    {
                        currwl.xrayordernumber = Convert.ToString(m_rd[9]).Trim();
                    }

                   
                    wllist.Add(currwl);
                }
                if (wllist != null && wllist.Count > 0)
                {
                    
                    updatelog("Get Hoxsp Worklist data found = " + wllist.Count.ToString());
                }
                else
                {
                    
                    updatelog("Not have Hosxp Worklist data");
                }
                
            }
            catch (Exception ex)
            {
                
                updatelog(ex.Message);
            }
            finally
            {
                if (m_con != null)
                {
                    m_con.Dispose();
                    m_con = null;
                }

                if (m_cmd != null)
                {
                    m_cmd.Dispose();
                    m_cmd = null;
                }
            }

            return wllist;
        }


        // method to generate HL7 ORM Message 01
        private string getxrayordermessage(CultureInfo cul, worklistdata wl)
        {
            string datetimeformat = "yyyyMMddhhmmss";

//            string orm_template = @"MSH|^~\&|hosxp2tpwl|hosxp2tpwl|TPACS|TPACS|{0}||ORM^O01^ORM_O01|{1}|P|2.5
//PID||{2}|{2}||{5}^{4}^^^{3}||{6}|{7}
//ORC|NW|{8}|{8}|{9}||||||{0}
//OBR|{8}|{8}|{8}|{8}|||||||||||||||{8}|{9}||||CR
//ZDS|1.2.840.114315.1.1.{11}^DCM4CHEE^TPACS";

            string orm_template = @"MSH|^~\&|hosxp2tpwl|hosxp2tpwl|TPACS|TPACS|{0}||ORM^O01^ORM_O01|{1}|P|2.5
PID||{2}|{2}||{5}^{4}^^^{3}||{6}|{7}
ORC|NW|{8}|{8}|{9}|||^^^{0}
OBR|1|||^^^^{10}^||||||||||||||{1}|{1}|{1}|||| CR||||||||||||||||||||
ZDS|1.2.840.114315.1.1.{11}^DCM4CHEE^TPACS";
            //0=orderdate
            //1=xray order number
            //2=hn
            //3=lname
            //4=fname
            //5=pname
            //6=birhtdate
            //7=sex
            //8=xray order number
            //9=xary code
            //10=xray_desc
            //11=datetimenow
            string result = String.Format(orm_template, wl.orderdate.ToString(datetimeformat, cul), wl.xrayordernumber, wl.hn, wl.lname, wl.fname, wl.pname, wl.birthday.ToString(datetimeformat, cul), wl.sex, wl.xrayordernumber, wl.xraycode,wl.xraydesc, DateTime.Now.ToString("yyyyMMddhhmmss.fff", cul));
            return result;
        }

        private void updatelog(string msg)
        {

            if (lstlog.InvokeRequired)
            {
                lstlog.Invoke(new MethodInvoker(delegate {

                    if (lstlog.Items.Count > 100)
                    {
                        lstlog.Items.RemoveAt(99);
                    }

                    lstlog.Items.Insert(0,DateTime.Now.ToString("yyyyMMdd HH:mm:ss",en_us) + ":"+ msg);
                    lstlog.Refresh();
                }));
            }
          
        }


        private void writetempfile(string value)
        {
            string currpath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                fs = new FileStream(Path.Combine(currpath, "tempcount.txt"), FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                sw = new StreamWriter(fs);
                sw.Write(value);
                sw.Flush();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sw.Close();
                fs.Close();
                sw.Dispose();
                fs.Dispose();
                sw = null;
                fs = null;
            }
        }

        private string readtempfile()
        {
            string result = null;
            string currpath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            FileStream fs = null;
            StreamReader rd = null;
            try
            {
                fs = new FileStream(Path.Combine(currpath, "tempcount.txt"), FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                rd = new StreamReader(fs);
                result=rd.ReadToEnd();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                rd.Close();
                rd = null;
                fs.Close();
                fs = null;
            }

            return result;
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    updatelog("Interval Start");

        //    List<worklistdata> result = getdatafromhosxp();

        //    if (result != null && result.Count >= 0) // have data
        //    {
        //        foreach (worklistdata wl in result)
        //        {
        //            //create msg orm 01
        //            string msgsend = getxrayordermessage(en_us, wl);
        //            updatelog(String.Format("Create Worklist msg Detail HN:{0} Name:{1}", wl.hn, wl.pname + " " + wl.fname + " " + wl.lname));
        //            //send msg to worklist server
        //            SendORM_Message(msgsend, wl);

        //        }

        //    }
        //}
    }
}
