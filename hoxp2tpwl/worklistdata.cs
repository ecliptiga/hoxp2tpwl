﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;


namespace hoxp2tpwl
{
    class worklistdata
    {   
        public string hn { set; get; }
        public string pname { set; get; }
        public string fname { set; get; }
        public string lname { set; get; }
        public DateTime birthday { set; get; }
        public string sex { set; get; }
        public string xraycode { set; get; }
        public string xraydesc { set; get; }
        public DateTime orderdate { set; get; }
        public string xrayordernumber { set; get; }
    }
}
